from os import listdir
from numpy import asarray
from numpy import vstack
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from numpy import savez_compressed

#nacitanie obrazkov
def load_images(path, size=(256,256)):
	data_list = list()
	for filename in listdir(path):
		pixels = load_img(path + filename, target_size=size)
		pixels = img_to_array(pixels)
		data_list.append(pixels)
	return asarray(data_list)

# nacitat z google drive
path = '/content/drive/MyDrive/data-final/'

#nacitat dataset A = bernese
dataA1 = load_images(path + 'bernese-train/')
dataAB = load_images(path + 'bernese-test/')
dataA = vstack((dataA1, dataAB))
print('Loaded dataA: ', dataA.shape)

#nacitat  dataset B = samoyed
dataB1 = load_images(path + 'samoyed-train/')
dataB2 = load_images(path + 'samoyed-test/')
dataB = vstack((dataB1, dataB2))
print('Loaded dataB: ', dataB.shape)

#ulozit do numpy array
filename = 'bernese-samoyed.npz'
savez_compressed(filename, dataA, dataB)
print('Saved dataset: ', filename)