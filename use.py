from numpy import load
from numpy import expand_dims
from keras.models import load_model
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from matplotlib import pyplot
 
#nacitanie obrazku
def load_image(filename, size=(256,256)):
	pixels = load_img(filename, target_size=size)
	pixels = img_to_array(pixels)
	pixels = expand_dims(pixels, 0)
	pixels = (pixels - 127.5) / 127.5
	return pixels
 
#nacitanie obrazku
image_src = load_image('/content/drive/MyDrive/data2/ezgif-frame-010.jpg')
#nacitanie modelu
cust = {'InstanceNormalization': InstanceNormalization}
model_AtoB = load_model('contentg_model_BtoA_006540.h5', cust)
#transformacia obrazku
image_tar = model_AtoB.predict(image_src)
image_tar = (image_tar + 1) / 2.0
#vykreslenie
pyplot.imshow(image_tar[0])
pyplot.show()